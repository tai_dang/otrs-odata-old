using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using NetCore.OData.Data;
using NetCore.OData.Extensions;
using NetCore.OData.Initializations;
using Serilog;
using System;
using Microsoft.Extensions.DependencyInjection;

namespace NetCore.OData
{
    public class Program
    {
        public static void Main(string[] args)
        {
            try
            {
                CreateHostBuilder(args)
                    .Build()
                    .MigrateDbContext<VodafoneContext>((context, services) =>
                    {
                        var setting = services.GetService<IOptionsSnapshot<AppSettings>>();
                        var logger = services.GetService<ILogger<TestDataInit>>();
                        new TestDataInit(context, setting, logger).InitAsync().Wait();
                    }).Run();
            }
            catch (Exception ex)
            {
                Log.Fatal(ex, "Host terminated unexpectedly");
            }
            finally
            {
                Log.CloseAndFlush();
            }
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .UseSerilog((hostingContext, loggerConfiguration) =>
                {
                    loggerConfiguration.ReadFrom.Configuration(hostingContext.Configuration);
                })
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    //webBuilder.UseUrls("https://0.0.0.0:44350", "http://0.0.0.0:5000")
                    //webBuilder.UseUrls("https://0.0.0.0:44350")
                    //            .UseStartup<Startup>();
                    webBuilder.UseStartup<Startup>();
                });
    }
}
