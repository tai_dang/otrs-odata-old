﻿namespace NetCore.OData
{
    using Microsoft.AspNetCore.Mvc.ApiExplorer;
    using Microsoft.Extensions.DependencyInjection;
    using Microsoft.Extensions.Options;
    using Microsoft.OpenApi.Models;
    using Swashbuckle.AspNetCore.SwaggerGen;
    using System.Collections.Generic;

    /// <summary>
    /// Configures the Swagger generation options.
    /// </summary>
    /// <remarks>This allows API versioning to define a Swagger document per API version after the
    /// <see cref="IApiVersionDescriptionProvider"/> service has been resolved from the service container.</remarks>
    public class ConfigureSwaggerOptions : IConfigureOptions<SwaggerGenOptions>
    {
        readonly IApiVersionDescriptionProvider provider;

        /// <summary>
        /// Initializes a new instance of the <see cref="ConfigureSwaggerOptions"/> class.
        /// </summary>
        /// <param name="provider">The <see cref="IApiVersionDescriptionProvider">provider</see> used to generate Swagger documents.</param>
        public ConfigureSwaggerOptions(IApiVersionDescriptionProvider provider)
            => this.provider = provider;

        /// <inheritdoc />
        public void Configure(SwaggerGenOptions options)
        {
            // Confugure Authentication schemes
            OpenApiSecurityScheme bearerScheme = new OpenApiSecurityScheme()
            {
                Name = "Bearer Authentication",
                BearerFormat = "JWT",
                Scheme = "bearer",
                Description = "Specify the Bearer authorization token",
                In = ParameterLocation.Header,
                Type = SecuritySchemeType.Http,
            };

            OpenApiSecurityScheme basicScheme = new OpenApiSecurityScheme()
            {
                Name = "Basic Authentication",
                BearerFormat = "JWT",
                Scheme = "basic",
                Description = "Specify the username and password",
                In = ParameterLocation.Header,
                Type = SecuritySchemeType.Http,
            };

            OpenApiSecurityRequirement bearerSecurityRequirements = new OpenApiSecurityRequirement()
                    {
                        {
                            new OpenApiSecurityScheme
                            {
                                Reference = new OpenApiReference
                                {
                                    Type = ReferenceType.SecurityScheme,
                                    Id = "bearer"
                                },
                                Scheme = "bearer",
                                In = ParameterLocation.Header
                            },
                            new List<string>()
                        }
                    };

            OpenApiSecurityRequirement basicSecurityRequirements = new OpenApiSecurityRequirement()
                    {
                        {
                            new OpenApiSecurityScheme
                            {
                                Reference = new OpenApiReference
                                {
                                    Type = ReferenceType.SecurityScheme,
                                    Id = "basic"
                                },
                                Scheme = "basic",
                                In = ParameterLocation.Header
                            },
                            new List<string>()
                        }
                    };

            options.AddSecurityDefinition("bearer", bearerScheme);
            options.AddSecurityRequirement(bearerSecurityRequirements);

            options.AddSecurityDefinition("basic", basicScheme);
            options.AddSecurityRequirement(basicSecurityRequirements);

            // add a swagger document for each discovered API version
            // note: you might choose to skip or document deprecated API versions differently
            foreach (var description in provider.ApiVersionDescriptions)
            {
                options.SwaggerDoc(description.GroupName, CreateInfoForApiVersion(description));
            }
        }

        static OpenApiInfo CreateInfoForApiVersion(ApiVersionDescription description)
        {
            var info = new OpenApiInfo()
            {
                Title = "OTRS OData API",
                Version = description.ApiVersion.ToString(),
                Description = "OData API for OTRS",
                Contact = new OpenApiContact() { Name = "Tai Dang", Email = "tai.dang@infodation.vn" },
                //License = new OpenApiLicense() { Name = "MIT", Url = new Uri( "https://opensource.org/licenses/MIT" ) }
            };

            if (description.IsDeprecated)
            {
                info.Description += " This API version has been deprecated.";
            }

            return info;
        }
    }
}