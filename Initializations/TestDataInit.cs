﻿using FizzWare.NBuilder;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using NetCore.OData.Data;
using NetCore.OData.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace NetCore.OData.Initializations
{
    public class TestDataInit
    {
        private readonly VodafoneContext _context;
        private readonly IOptionsSnapshot<AppSettings> _settings;
        private readonly ILogger<TestDataInit> _logger;
        public TestDataInit(VodafoneContext context,
                            IOptionsSnapshot<AppSettings> settings,
                                    ILogger<TestDataInit> logger)
        {
            _context = context;
            _settings = settings;
            _logger = logger;
        }

        public async Task InitAsync()
        {
            if (!_settings.Value.InitData)
                return;

            var data = GenerateTickets();
            _logger.LogInformation("Init test data [{@Ticket}]", data);

            _context.Ticket.AddRange(data);

            await _context.SaveChangesAsync();
        }

        private IList<Ticket> GenerateTickets()
        {
            return Builder<Ticket>.CreateListOfSize(100).Build();
        }
    }
}
