﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NetCore.OData.Models
{
    public class TicketState
    {
        public short id { get; set; }
        public string name { get; set; }
        public string comments { get; set; }
        public int type_id { get; set; }
        public int valid_id { get; set; }
        public DateTime create_time { get; set; }
        public int create_by { get; set; }
        public DateTime change_time { get; set; }
        public int change_by { get; set; }
    }
}
