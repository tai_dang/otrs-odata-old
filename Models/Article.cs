﻿using Microsoft.AspNet.OData.Builder;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NetCore.OData.Models
{
    public class Article
    {
        [JsonProperty("ArticleID")]
        public int Id { get; set; }

        [JsonProperty("Subject")]
        public string Subject { get; set; }

        [JsonProperty("Body")]
        public string Body { get; set; }

        [Contained]
        public List<Attachment> Attachments { get; set; }

        public IDictionary<string, object> DynamicProperties { get; set; }
    }
}
