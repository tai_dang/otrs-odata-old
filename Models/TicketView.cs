﻿using Newtonsoft.Json;
using System;

namespace NetCore.OData.Models
{
    public partial class TicketView
    {
        //public TicketView() { }
        //public TicketView(TicketView ticket)
        //{
        //    this.Id = ticket.Id;
        //    this.TicketNumber = ticket.TicketNumber;
        //    this.Title = ticket.Title;
        //    this.Queue = ticket.Queue;
        //    this.State = ticket.State;
        //    this.StateType = ticket.StateType;
        //    this.UserId = ticket.UserId;
        //    this.CustomerCode = ticket.CustomerCode;
        //    this.CreateTime = ticket.CreateTime;
        //}
        public long Id { get; set; }
        public string TicketNumber { get; set; }
        public string Title { get; set; }
        public string Queue { get; set; }
        public string State { get; set; }
        public string StateType { get; set; }
        public int UserId { get; set; }
        public string CustomerCode { get; set; }
        public DateTime CreateTime { get; set; }
    }
}
