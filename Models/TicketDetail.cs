﻿using Microsoft.AspNet.OData.Query;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NetCore.OData.Models
{
    //public class TicketDetail : TicketView
    public class TicketDetail
    {
        //public TicketDetail(TicketView ticket)
        //    : base(ticket)
        //{ }
        public long Id { get; set; }
        public string TicketNumber { get; set; }
        public string Title { get; set; }
        public string Queue { get; set; }
        public string State { get; set; }
        public string StateType { get; set; }
        public int UserId { get; set; }
        public string CustomerCode { get; set; }
        public DateTime CreateTime { get; set; }
        public List<Article> Articles { get; set; }
        public IDictionary<string, object> DynamicProperties { get; set; }

        internal static TicketDetail fromTicketView(TicketView ticketView)
        {
            TicketDetail ticket = new TicketDetail();
            ticket.Id = ticketView.Id;
            ticket.TicketNumber = ticketView.TicketNumber;
            ticket.Title = ticketView.Title;
            ticket.Queue = ticketView.Queue;
            ticket.State = ticketView.State;
            ticket.StateType = ticketView.StateType;
            ticket.UserId = ticketView.UserId;
            ticket.CustomerCode = ticketView.CustomerCode;
            ticket.CreateTime = ticketView.CreateTime;

            return ticket;
        }
    }
}
