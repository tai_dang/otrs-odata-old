﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NetCore.OData.Models
{
    public class Queue
    {
        public int id { get; set; }
        public string name { get; set; }
        public int group_id { get; set; }
        public int unlock_timeout { get; set; }
        public int first_response_time { get; set; }
        public int first_response_notify { get; set; }
        public int update_time { get; set; }
        public int update_notify { get; set; }
        public int solution_time { get; set; }
        public int solution_notify { get; set; }
        public int system_address_id { get; set; }
        public string calendar_name { get; set; }
        public string default_sign_key { get; set; }
        public int salutation_id { get; set; }
        public int signature_id { get; set; }
        public int follow_up_id { get; set; }
        public int follow_up_lock { get; set; }
        public string comments { get; set; }
        public int valid_id { get; set; }
        public DateTime create_time { get; set; }
        public int create_by { get; set; }
        public DateTime change_time { get; set; }
        public int change_by { get; set; }
    }
}
