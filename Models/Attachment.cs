﻿using Microsoft.AspNet.OData.Builder;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NetCore.OData.Models
{
    [MediaType]
    public class Attachment
    {
        public int Id { get; set; }
        public string Filename { get; set; }
        public string Filesize { get; set; }
        public string ContentType { get; set; }
        public string Disposition { get; set; }
    }
}
