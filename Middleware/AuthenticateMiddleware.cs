﻿using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore.Internal;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OData;
using NetCore.OData.Helpers;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace NetCore.OData.Middleware
{
    public class AuthenticateMiddleware
    {
        private RequestDelegate next;

        public static byte[] SecretKey;
        public AuthenticateMiddleware(RequestDelegate nextDelegate)
        {
            next = nextDelegate;
        }
        public async Task Invoke(HttpContext context)
        {
            if (context.Request.Path.StartsWithSegments("/swagger"))
            {
                await next(context);
                return;
            }

            string authorizationHeader = context.Request.Headers["Authorization"].ToString();

            if (authorizationHeader.StartsWith("Bearer "))
            {
                JwtSecurityTokenHandler handler = new JwtSecurityTokenHandler();

                string token = authorizationHeader.Substring("Bearer ".Length);

                if (handler.CanReadToken(token))
                {
                    var jwtToken = handler.ReadToken(token) as JwtSecurityToken;
                    if (jwtToken.Claims.Any(claim => claim.Type == "Username"))
                    {
                        //string username = jwtToken.Claims.First(claim => claim.Type == "Username").Value;
                        await next(context);
                        return;
                    }
                }
            }

            if (authorizationHeader.StartsWith("Basic "))
            {
                string base64Encoded = authorizationHeader.Substring("Basic ".Length);
                byte[] data;

                try
                {
                    data = Convert.FromBase64String(base64Encoded);
                }
                catch
                {
                    await Write401Response(context, "Base64 decode error");
                    return;
                }
                
                string userpass = Encoding.ASCII.GetString(data);
                var userpassSplitted = userpass.Split(':');
                if (userpassSplitted.Length >= 2)
                {
                    string username = userpassSplitted[0];
                    string password = string.Join(':', userpassSplitted.Skip(1));
                    if (ScriptExecutor.UserAuthenticate(username, password))
                    {
                        JwtSecurityTokenHandler handler = new JwtSecurityTokenHandler();

                        SecurityTokenDescriptor descriptor = new SecurityTokenDescriptor
                        {
                            Subject = new ClaimsIdentity(new Claim[] {
                                                    new Claim("Username", username)
                            }),
                            Expires = DateTime.UtcNow.AddHours(24),
                            SigningCredentials = new SigningCredentials(
                                new SymmetricSecurityKey(SecretKey),
                                SecurityAlgorithms.HmacSha256Signature)
                        };
                        SecurityToken token = handler.CreateToken(descriptor);
                        context.Response.Headers["Token"] = handler.WriteToken(token);

                        await next(context);
                        return;
                    }
                }
            }

            await Write401Response(context, "Authentication failed");
        }
        private async Task Write401Response(HttpContext context, string message)
        {
            var error = new ODataError
            {
                ErrorCode = "401 Unauthorized",
                Message = message,
                Target = "Authentication",
            };

            context.Response.StatusCode = StatusCodes.Status401Unauthorized;
            await context.Response.WriteAsync(error.ToString());
        }
    }
}
