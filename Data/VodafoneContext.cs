﻿using Microsoft.EntityFrameworkCore;
using NetCore.OData.Models;

namespace NetCore.OData.Data
{
    public partial class VodafoneContext : DbContext
    {
        public VodafoneContext(DbContextOptions<VodafoneContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Ticket> Ticket { get; set; }
        public virtual DbSet<Queue> Queue { get; set; }
        public virtual DbSet<TicketState> TicketState { get; set; }
        public virtual DbSet<TicketStateType> TicketStateType { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Ticket>(entity =>
            {
                entity.ToTable("ticket");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.ArchiveFlag)
                    .HasColumnName("archive_flag")
                    .HasColumnType("smallint(6)");

                entity.Property(e => e.ChangeBy)
                    .HasColumnName("change_by")
                    .HasColumnType("int(11)");

                entity.Property(e => e.ChangeTime)
                    .HasColumnName("change_time")
                    .HasColumnType("datetime");

                entity.Property(e => e.CreateBy)
                    .HasColumnName("create_by")
                    .HasColumnType("int(11)");

                entity.Property(e => e.CreateTime)
                    .HasColumnName("create_time")
                    .HasColumnType("datetime");

                entity.Property(e => e.CreateTimeUnix)
                    .HasColumnName("create_time_unix")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.CustomerId)
                    .HasColumnName("customer_id")
                    .HasColumnType("varchar(150)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.CustomerUserId)
                    .HasColumnName("customer_user_id")
                    .HasColumnType("varchar(250)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.EscalationResponseTime)
                    .HasColumnName("escalation_response_time")
                    .HasColumnType("int(11)");

                entity.Property(e => e.EscalationSolutionTime)
                    .HasColumnName("escalation_solution_time")
                    .HasColumnType("int(11)");

                entity.Property(e => e.EscalationTime)
                    .HasColumnName("escalation_time")
                    .HasColumnType("int(11)");

                entity.Property(e => e.EscalationUpdateTime)
                    .HasColumnName("escalation_update_time")
                    .HasColumnType("int(11)");

                entity.Property(e => e.QueueId)
                    .HasColumnName("queue_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.ResponsibleUserId)
                    .HasColumnName("responsible_user_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.ServiceId)
                    .HasColumnName("service_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.SlaId)
                    .HasColumnName("sla_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.TicketLockId)
                    .HasColumnName("ticket_lock_id")
                    .HasColumnType("smallint(6)");

                entity.Property(e => e.TicketPriorityId)
                    .HasColumnName("ticket_priority_id")
                    .HasColumnType("smallint(6)");

                entity.Property(e => e.TicketStateId)
                    .HasColumnName("ticket_state_id")
                    .HasColumnType("smallint(6)");

                entity.Property(e => e.Timeout)
                    .HasColumnName("timeout")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Title)
                    .HasColumnName("title")
                    .HasColumnType("varchar(255)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.Tn)
                    .IsRequired()
                    .HasColumnName("tn")
                    .HasColumnType("varchar(50)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.TypeId)
                    .HasColumnName("type_id")
                    .HasColumnType("smallint(6)");

                entity.Property(e => e.UntilTime)
                    .HasColumnName("until_time")
                    .HasColumnType("int(11)");

                entity.Property(e => e.UserId)
                    .HasColumnName("user_id")
                    .HasColumnType("int(11)");
            });

            modelBuilder.Entity<Queue>(entity =>
            {
                entity.ToTable("queue");
            });

            modelBuilder.Entity<TicketState>(entity =>
            {
                entity.ToTable("ticket_state");
            });

            modelBuilder.Entity<TicketStateType>(entity =>
            {
                entity.ToTable("ticket_state_type");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
