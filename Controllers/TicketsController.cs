﻿using Microsoft.AspNet.OData;
using Microsoft.AspNet.OData.Routing;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Microsoft.OData;
using NetCore.OData.Data;
using NetCore.OData.Helpers;
using NetCore.OData.Models;
using Swashbuckle.AspNetCore.Annotations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using static Microsoft.AspNetCore.Http.StatusCodes;

namespace NetCore.OData.Controllers
{
    /// <summary>
    /// Gets all tickets, support many query parameters.
    /// </summary>
    [ApiVersion("1.0")]
    public class TicketsController : ODataController
    {
        private readonly ILogger<TicketsController> _logger;
        private readonly VodafoneContext _context;
        public TicketsController(VodafoneContext context,
                                    ILogger<TicketsController> logger)
        {
            _context = context;
            _logger = logger;
        }

        /// <summary>
        /// Gets all tickets.
        /// </summary>
        /// <returns>All available tickets.</returns>
        /// <response code="200">The successfully retrieved tickets.</response>
        [HttpGet]
        [EnableQuery]
        [SwaggerOperation(
           Summary = "Get tickets",
           Description = "Get tickets")]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.InternalServerError)]
        [ProducesResponseType((int)HttpStatusCode.Unauthorized)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public IQueryable<TicketView> Get()
        {
            var query = from Ticket in _context.Ticket
                        join Queue in _context.Queue on Ticket.QueueId equals Queue.id
                        join TicketState in _context.TicketState on Ticket.TicketStateId equals TicketState.id
                        join TicketStateType in _context.TicketStateType on TicketState.type_id equals TicketStateType.id
                        select new TicketView
                        {
                            Id = Ticket.Id,
                            TicketNumber = Ticket.Tn,
                            Title = Ticket.Title,
                            Queue = Queue.name,
                            State = TicketState.name,
                            StateType = TicketStateType.name,
                            UserId = Ticket.UserId,
                            CustomerCode = Ticket.CustomerUserId,
                            CreateTime = Ticket.CreateTime
                        };
            return query;
        }

        /*






        //[HttpPost]
        //[ODataRoute("tickets")]
        //[Route("tickets")]
        //public async Task<IActionResult> Post([FromBody] TicketView ticket)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return BadRequest(ModelState);
        //    }

        //    _context.Tickets.Add(ticket);
        //    await _context.SaveChangesAsync();
        //    return Created(ticket);
        //}

        //[HttpPatch]
        //[ODataRoute("tickets({ticketId})")]
        //public IActionResult Patch([FromODataUri] long ticketId,
        //    [FromBody] Delta<TicketDetail> delta)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        _logger.LogInformation("Model State is invalid!");
        //        var errors = ModelState.Values.SelectMany(v => v.Errors);
        //        foreach (var error in errors)
        //        {
        //            _logger.LogInformation($"Error: {error.ErrorMessage}");
        //        }
        //        return BadRequest(ModelState);
        //    }

        //    _logger.LogInformation("Model State is valid!");

        //    var properties = delta.GetChangedPropertyNames();

        //    if (!properties.Contains("State"))
        //    {
        //        _logger.LogInformation("Request does not contain State!");
        //        return BadRequest("Request does not contain State!");
        //    }

        //    if (!properties.Contains("UserId"))
        //    {
        //        _logger.LogInformation("Request does not contain UserId!");
        //        return BadRequest("Request does not contain UserId!");
        //    }

        //    object result;

        //    delta.TryGetPropertyValue("State", out result);
        //    string state = (string)result;

        //    delta.TryGetPropertyValue("UserId", out result);
        //    int userId = (int)result;

        //    string command = $"/opt/otrs/Custom/update_ticket_state.pl '{state}' {ticketId} {userId}";

        //    string outputString = ScriptExecutor.Execute(command);

        //    if (outputString.Contains("Update unsuccess"))
        //    {
        //        return BadRequest("Invalid request");
        //    }
        //    else
        //    {
        //        return NoContent();
        //    }

        //    //var entity = await _context.Tickets.FindAsync(key);
        //    //if (entity == null)
        //    //{
        //    //    return NotFound();
        //    //}
        //    //ticket.Patch(entity);
        //    //try
        //    //{
        //    //    await _context.SaveChangesAsync();
        //    //}
        //    //catch (DbUpdateConcurrencyException)
        //    //{
        //    //    if (!Exists(key))
        //    //    {
        //    //        return NotFound();
        //    //    }
        //    //    else
        //    //    {
        //    //        throw;
        //    //    }
        //    //}

        //    //return Updated(entity);
        //}*/

        private TicketView _GetTicket(long ticketId)
        {
            return Get().FirstOrDefault((x) => x.Id == ticketId);
        }

        private TicketDetail _GetTicketDetail(long ticketId)
        {
            var ticketView = _GetTicket(ticketId);

            if (ticketView == null)
            {
                return null;
            }

            var ticketDetail = TicketDetail.fromTicketView(ticketView);

            var jObject = ScriptExecutor.GetSingleTicket(ticketId);

            ticketDetail.DynamicProperties = new Dictionary<string, object>();

            foreach (var item in jObject)
            {
                if (item.Key.StartsWith("DynamicField_"))
                {
                    ticketDetail.DynamicProperties[item.Key.Substring(13)] = item.Value.ToString();
                }
            }

            ticketDetail.Articles = ScriptExecutor.GetArticles(ticketId);

            return ticketDetail;
        }

        //[HttpPut]
        //[ODataRoute("tickets")]
        //[Route("tickets")]
        //public async Task<IActionResult> Put([FromODataUri] int key, [FromBody] TicketView update)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return BadRequest(ModelState);
        //    }
        //    if (key != update.Id)
        //    {
        //        return BadRequest();
        //    }
        //    _context.Entry(update).State = EntityState.Modified;
        //    try
        //    {
        //        await _context.SaveChangesAsync();
        //    }
        //    catch (DbUpdateConcurrencyException)
        //    {
        //        if (!Exists(key))
        //        {
        //            return NotFound();
        //        }
        //        else
        //        {
        //            throw;
        //        }
        //    }
        //    return Updated(update);
        //}

        //[HttpDelete]
        //[ODataRoute("tickets")]
        //[Route("tickets")]
        //public async Task<ActionResult> Delete([FromODataUri] int key)
        //{
        //    var ticket = await _context.Tickets.FindAsync(key);
        //    if (ticket == null)
        //    {
        //        return NotFound();
        //    }
        //    _context.Tickets.Remove(ticket);
        //    await _context.SaveChangesAsync();
        //    return StatusCode((int)HttpStatusCode.NoContent);
        //}

        //private bool Exists(int key)
        //{
        //    return _context.Tickets.Any(x => x.Id == key);
        //}
    }
}
