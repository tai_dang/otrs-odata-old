﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNet.OData;
using Microsoft.AspNet.OData.Routing;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.OData;
using NetCore.OData.Helpers;
using NetCore.OData.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace NetCore.OData.Controllers
{
    /// <summary>
    /// Get article and its attachments.
    /// </summary>
    [ApiVersion("1.0")]
    public class ArticleController : ODataController
    {
        private readonly ILogger<ArticleController> _logger;

        public ArticleController(ILogger<ArticleController> logger)
        {
            _logger = logger;
        }

        [HttpGet]
        //[ODataRoute("article({articleId})")]
        [EnableQuery]
        public IActionResult Get(int key)
        {
            Article article = ScriptExecutor.GetSingleArticle(key);

            if (article != null)
            {
                return Ok(article);
            }
            else
            {
                var error = new ODataError
                {
                    ErrorCode = "400 Bad Request",
                    Message = "Article not found",
                    Target = "article"
                };
                return BadRequest(error);
            }
        }

        [HttpGet]
        //[ODataRoute("article({articleId})/attachments")]
        [EnableQuery]
        public IActionResult GetAttachments(int key)
        {
            var article = ScriptExecutor.GetSingleArticle(key);

            if (article != null)
            {
                return Ok(article.Attachments);
            }
            else
            {
                var error = new ODataError
                {
                    ErrorCode = "400 Bad Request",
                    Message = "Article not found",
                    Target = "article"
                };
                return BadRequest(error);
            }
        }

        [HttpGet]
        [ODataRoute("Article({articleId})/Attachments({attachmentId})")]
        [EnableQuery]
        public IActionResult GetSingleAttachment([FromODataUri] int articleId,
                                        [FromODataUri] int attachmentId)
        {
            var attachment = ScriptExecutor.GetSingleAttachment(articleId, attachmentId);

            if (attachment == null)
            {
                var error = new ODataError
                {
                    ErrorCode = "400 Bad Request",
                    Message = "Attachment not found",
                    Target = "attachment"
                };
                return BadRequest(error);
            }
            else
            {
                return Ok(attachment);
            }
        }

        [HttpGet]
        [ODataRoute("Article({articleId})/Attachments({attachmentId})/$value")]
        [EnableQuery]
        public IActionResult GetAttachmentStream([FromODataUri] int articleId,
                                                [FromODataUri] int attachmentId)
        {
            Attachment attachment = ScriptExecutor.GetSingleAttachment(articleId, attachmentId);

            Stream output = ScriptExecutor.GetAttachmentStream(articleId, attachmentId);

            return File(output, attachment.ContentType, attachment.Filename);
        }
    }
}
