﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNet.OData;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.OData;
using NetCore.OData.Data;
using NetCore.OData.Helpers;
using NetCore.OData.Models;
using static Microsoft.AspNetCore.Http.StatusCodes;

namespace NetCore.OData.Controllers
{
    /// <summary>
    /// Get article and its attachments.
    /// </summary>
    [ApiVersion("1.0")]
    public class TicketController : ODataController
    {
        private readonly ILogger<TicketController> _logger;
        private readonly VodafoneContext _context;
        public TicketController(VodafoneContext context,
                                    ILogger<TicketController> logger)
        {
            _context = context;
            _logger = logger;
        }

        [HttpGet]
        //[ODataRoute("tickets({ticketId})")]
        [EnableQuery]
        [Produces("application/json")]
        [ProducesResponseType(typeof(TicketDetail), Status200OK)]
        [ProducesResponseType(Status401Unauthorized)]
        [ProducesResponseType(Status404NotFound)]
        public IActionResult Get(long key)
        {
            var ticket = _GetTicketDetail(key);

            if (ticket == null)
            {
                var error = new ODataError
                {
                    ErrorCode = "404 Not Found",
                    Message = "Ticket not found",
                    Target = "Ticket"
                };
                return NotFound(error);
            }

            return Ok(ticket);
        }

        private TicketView _GetTicket(long ticketId)
        {
            var query = from Ticket in _context.Ticket
                        join Queue in _context.Queue on Ticket.QueueId equals Queue.id
                        join TicketState in _context.TicketState on Ticket.TicketStateId equals TicketState.id
                        join TicketStateType in _context.TicketStateType on TicketState.type_id equals TicketStateType.id
                        select new TicketView
                        {
                            Id = Ticket.Id,
                            TicketNumber = Ticket.Tn,
                            Title = Ticket.Title,
                            Queue = Queue.name,
                            State = TicketState.name,
                            StateType = TicketStateType.name,
                            UserId = Ticket.UserId,
                            CustomerCode = Ticket.CustomerUserId,
                            CreateTime = Ticket.CreateTime
                        };

            return query.FirstOrDefault((x) => x.Id == ticketId);
        }

        private TicketDetail _GetTicketDetail(long ticketId)
        {
            var ticketView = _GetTicket(ticketId);

            if (ticketView == null)
            {
                return null;
            }

            var ticketDetail = TicketDetail.fromTicketView(ticketView);

            var jObject = ScriptExecutor.GetSingleTicket(ticketId);

            ticketDetail.DynamicProperties = new Dictionary<string, object>();

            foreach (var item in jObject)
            {
                if (item.Key.StartsWith("DynamicField_"))
                {
                    ticketDetail.DynamicProperties[item.Key.Substring(13)] = item.Value.ToString();
                }
            }

            ticketDetail.Articles = ScriptExecutor.GetArticles(ticketId);

            return ticketDetail;
        }

        [HttpGet]
        //[ODataRoute("tickets({ticketId})/articles")]
        [EnableQuery]
        public IActionResult GetArticles(long key)
        {
            var ticketView = _GetTicket(key);

            if (ticketView == null)
            {
                var error = new ODataError
                {
                    ErrorCode = "400 Bad Request",
                    Message = "Ticket not found",
                    Target = "tickets"
                };
                return BadRequest(error);
            }

            List<Article> result = ScriptExecutor.GetArticles(key);

            return Ok(result);
        }

        [HttpPost]
        //[ODataRoute("tickets({ticketId})/ActionCommon")]
        public IActionResult ActionCommon(long key, [FromBody] object content)
        {
            var ticketView = _GetTicket(key);

            if (ticketView == null)
            {
                var error = new ODataError
                {
                    ErrorCode = "400 Bad Request",
                    Message = "Ticket not found",
                    Target = "tickets"
                };
                return BadRequest(error);
            }

            _logger.LogInformation($"jsonString = {content}");

            string result = ScriptExecutor.Execute($"/opt/otrs/Custom/action_common.pl {key}", content.ToString(), out string log);

            if (result == "1" && Utils.GetODataErrorDetail(log).Count == 0)
            {
                var ticket = _GetTicketDetail(key);
                if (ticket == null)
                {
                    var error = new ODataError
                    {
                        ErrorCode = "500 Internal Server Error",
                        Message = "Cannot get updated ticket information",
                        Target = "tickets",
                        Details = Utils.GetODataErrorDetail(log),
                        InnerError = new ODataInnerError
                        {
                            Message = "Error",
                            StackTrace = log
                        }
                    };
                    return StatusCode(500, error);
                }
                else
                {
                    return Updated(ticket);
                }
            }
            else
            {
                var error = new ODataError
                {
                    ErrorCode = "400 Bad Request",
                    Message = "Update ticket not successful",
                    Target = "tickets",
                    Details = Utils.GetODataErrorDetail(log),
                    InnerError = new ODataInnerError
                    {
                        Message = "Error",
                        StackTrace = log
                    }
                };
                return BadRequest(error);
            }
        }

        [HttpPost]
        public IActionResult CreateTicket([FromBody] object content)
        {
            _logger.LogInformation($"jsonString = {content}");

            string result = ScriptExecutor.Execute($"/opt/otrs/Custom/create_ticket.pl", content.ToString(), out string log);

            if (int.TryParse(result, out int ticketId) && ticketId > 0)
            {
                var ticket = _GetTicketDetail(ticketId);
                if (ticket == null)
                {
                    var error = new ODataError
                    {
                        ErrorCode = "500 Internal Server Error",
                        Message = "Create new ticket not successful",
                        Target = "tickets",
                        Details = Utils.GetODataErrorDetail(log),
                        InnerError = new ODataInnerError
                        {
                            Message = "Error",
                            StackTrace = log
                        }
                    };
                    return StatusCode(500, error);
                }
                else
                {
                    //return Created<TicketDetail>(ticket);
                    return Created($"tickets({ticketId})", ticket);
                }
            }
            else
            {
                var error = new ODataError
                {
                    ErrorCode = "400 Bad Request",
                    Message = "Create new ticket not successful",
                    Target = "tickets",
                    Details = Utils.GetODataErrorDetail(log),
                    InnerError = new ODataInnerError
                    {
                        Message = "Error",
                        StackTrace = log
                    }
                };
                return BadRequest(error);
            }
        }
    }
}
