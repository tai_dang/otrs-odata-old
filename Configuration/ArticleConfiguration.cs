﻿using Microsoft.AspNet.OData.Builder;
using Microsoft.AspNetCore.Mvc;
using NetCore.OData.Models;

namespace NetCore.OData.Configuration
{
    public class ArticleConfiguration : IModelConfiguration
    {
        public void Apply(ODataModelBuilder builder, ApiVersion apiVersion)
        {
            var article = builder.EntitySet<Article>("Article");

            article.EntityType.Count().Filter().OrderBy().Select().Expand().Page(100, 100);
        }
    }
}