﻿using Microsoft.AspNet.OData.Builder;
using Microsoft.AspNetCore.Mvc;
using NetCore.OData.Models;

namespace NetCore.OData.Configuration
{
    public class TicketDetailConfiguration : IModelConfiguration
    {
        public void Apply(ODataModelBuilder builder, ApiVersion apiVersion)
        {
            var ticket = builder.EntitySet<TicketDetail>("Ticket");

            ticket.EntityType.Count().Filter().OrderBy().Select().Expand().Page(100, 100);
            ticket.EntityType.Action("ActionCommon").ReturnsFromEntitySet<TicketDetail>("Ticket");
            ticket.EntityType.Collection.Action("CreateTicket").ReturnsFromEntitySet<TicketDetail>("Ticket");
        }
    }
}