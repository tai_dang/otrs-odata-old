﻿using Microsoft.AspNet.OData.Builder;
using Microsoft.AspNetCore.Mvc;
using NetCore.OData.Models;

namespace NetCore.OData.Configuration
{
    public class TicketViewConfiguration : IModelConfiguration
    {
        public void Apply(ODataModelBuilder builder, ApiVersion apiVersion)
        {
            var tickets = builder.EntitySet<TicketView>("Tickets");
            tickets.EntityType.Count().Filter().OrderBy().Select().Page(100, 100);
        }
    }
}