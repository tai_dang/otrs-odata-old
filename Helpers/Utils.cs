﻿using Microsoft.OData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace NetCore.OData.Helpers
{
    public class Utils
    {
        public static List<ODataErrorDetail> GetODataErrorDetail(string text)
        {
            MatchCollection mc = Regex.Matches(text, @"\n Message: (.+)");

            var errorDetails = new List<ODataErrorDetail>();

            foreach (Match m in mc)
            {
                errorDetails.Add(new ODataErrorDetail
                {
                    ErrorCode = "OTRS error",
                    Message = m.Groups[1].Value
                });
            }

            return errorDetails;
        }
    }
}
