﻿using NetCore.OData.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;

namespace NetCore.OData.Helpers
{
    public class ScriptExecutor
    {
        private static readonly JsonSerializer _serializer = new JsonSerializer();
        public static JObject GetSingleTicket(long ticketId)
        {
            string ticketResult = Execute($"/opt/otrs/Custom/get_single_ticket.pl {ticketId}");

            JsonTextReader reader = new JsonTextReader(new StringReader(ticketResult));

            return _serializer.Deserialize<JObject>(reader);
        }

        public static List<Article> GetArticles(long ticketId)
        {
            string command = $"/opt/otrs/Custom/get_article.pl {ticketId}";

            string outputString = Execute(command);

            JsonTextReader reader = new JsonTextReader(new StringReader(outputString));

            return _serializer.Deserialize<List<Article>>(reader);
        }

        public static Article GetSingleArticle(int articleId)
        {
            if (articleId <= 0)
            {
                return null;
            }

            string command = $"/opt/otrs/Custom/get_single_article.pl {articleId}";

            string outputString = Execute(command);

            JsonTextReader reader = new JsonTextReader(new StringReader(outputString));
            var jObject = _serializer.Deserialize<JObject>(reader);

            Article article = jObject.ToObject<Article>();

            if (article.Id != articleId)
            {
                return null;
            }

            article.DynamicProperties = new Dictionary<string, object>();

            foreach (var item in jObject)
            {
                if (item.Key.StartsWith("DynamicField_"))
                {
                    article.DynamicProperties[item.Key.Substring(13)] = item.Value.ToString();
                }
            }

            article.Attachments = GetArticleAttachments(articleId);

            return article;
        }

        public static Attachment GetSingleAttachment(int articleId, int attachmentId)
        {
            string outputString = Execute($"/opt/otrs/Custom/get_single_attachment.pl {articleId} {attachmentId}");

            JsonTextReader reader = new JsonTextReader(new StringReader(outputString));

            var jObject = _serializer.Deserialize<JObject>(reader);

            if (jObject.Count == 0)
            {
                return null;
            }

            Attachment attachment = jObject.ToObject<Attachment>();

            attachment.Id = attachmentId;

            return attachment;
        }

        public static Stream GetAttachmentStream(int articleId, int attachmentId)
        {
            return GetStreamExecute($"/opt/otrs/Custom/get_attachment_content.pl {articleId} {attachmentId}");
        }

        public static bool UserAuthenticate(string username, string password)
        {
            var result = Execute($"/opt/otrs/Custom/auth.pl \"{username}\" \"{password}\"");

            return result == "1";
        }

        private static string Execute(string command)
        {
            Process proc = new Process
            {
                StartInfo = new ProcessStartInfo
                {
                    FileName = "/bin/bash",
                    Arguments = "-c \"" + command + "\"",
                    UseShellExecute = false,
                    RedirectStandardOutput = true,
                    CreateNoWindow = true
                }
            };

            proc.Start();

            return proc.StandardOutput.ReadToEnd();
        }

        private static Stream GetStreamExecute(string command)
        {
            Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);

            Process proc = new Process
            {
                StartInfo = new ProcessStartInfo
                {
                    FileName = "/bin/bash",
                    Arguments = "-c \"" + command + "\"",
                    UseShellExecute = false,
                    RedirectStandardOutput = true,
                    CreateNoWindow = true
                }
            };

            proc.Start();

            return proc.StandardOutput.BaseStream;
        }

        public static string Execute(string command, string input, out string error)
        {
            Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);

            Process proc = new Process
            {
                StartInfo = new ProcessStartInfo
                {
                    FileName = "/bin/bash",
                    Arguments = "-c \"" + command + "\"",
                    UseShellExecute = false,
                    RedirectStandardInput = true,
                    RedirectStandardOutput = true,
                    RedirectStandardError = true,
                    CreateNoWindow = true
                }
            };

            proc.Start();

            proc.StandardInput.Write(input);
            proc.StandardInput.Close();

            error = proc.StandardError.ReadToEnd();

            return proc.StandardOutput.ReadToEnd();
        }

        private static List<Attachment> GetArticleAttachments(int articleId)
        {
            string outputString = Execute($"/opt/otrs/Custom/get_attachment_indexes.pl {articleId}");

            JsonTextReader reader = new JsonTextReader(new StringReader(outputString));

            var jObject1 = _serializer.Deserialize<JObject>(reader);

            var attachments = new List<Attachment>();

            foreach (var item in jObject1)
            {
                Attachment attachment = item.Value.ToObject<Attachment>();
                attachment.Id = int.Parse(item.Key);
                attachments.Add(attachment);
            }

            return attachments;
        }
    }
}
