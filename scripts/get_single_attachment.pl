#!/usr/bin/perl

use strict;
use warnings;
use JSON;

use lib '/opt/otrs';

use Kernel::System::ObjectManager;

my $ArticleID = shift;
my $FileID = shift;

# create object manager
local $Kernel::OM = Kernel::System::ObjectManager->new(
    'Kernel::System::Log' => {
        LogPrefix => 'OTRS-otrs.get_attachment_indexes.pl',
    },
);

my $TicketObject = $Kernel::OM->Get('Kernel::System::Ticket');

my %result = $TicketObject->ArticleAttachment(
	ArticleID => $ArticleID,
	FileID => $FileID,
	UserID => 1,
);

#my $json = encode_json \%result;
my $json = JSON->new->utf8->encode(\%result);

print $json;
