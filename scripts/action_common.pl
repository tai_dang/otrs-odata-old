#!/usr/bin/perl

use strict;
use warnings;
use diagnostics;
use Test::MockObject;
use Test::MockObject::Extends;
use JSON;

use lib '/opt/otrs';
use lib '/opt/otrs/Custom';

use Kernel::System::ObjectManager;
use Kernel::Modules::AgentTicketActionCommon;

my $TicketID = shift;

my @lines;
while (<>) {
    s/\s+\z//;
    push @lines, $_;
}

my $input = join " ", @lines;

my $ParamsRef = JSON->new->utf8->decode($input);

my %ActionParams = %{$$ParamsRef{ActionParams}};
delete $$ParamsRef{ActionParams};
$ActionParams{TicketID} = $TicketID;

# create object manager
local $Kernel::OM = Kernel::System::ObjectManager->new(
    'Kernel::System::Log' => {
        LogPrefix => 'OTRS-otrs.create_note.pl',
    },
);

$ActionParams{ParamObject} = $Kernel::OM->Get('Kernel::System::Web::Request');
$ActionParams{ParamObject}->{Query} = Test::MockObject::Extends->new($ActionParams{ParamObject}->{Query});
$ActionParams{DBObject} = $Kernel::OM->Get('Kernel::System::DB');
$ActionParams{TicketObject} = $Kernel::OM->Get('Kernel::System::Ticket');
my $LayoutObject = $Kernel::OM->Get('Kernel::Output::HTML::Layout');
$ActionParams{LayoutObject} = Test::MockObject::Extends->new($LayoutObject);
$ActionParams{LogObject} = $Kernel::OM->Get('Kernel::System::Log');
$ActionParams{QueueObject} = $Kernel::OM->Get('Kernel::System::Queue');
$ActionParams{ConfigObject} = $Kernel::OM->Get('Kernel::Config');
$ActionParams{EncodeObject} = $Kernel::OM->Get('Kernel::System::Encode');
$ActionParams{UserObject} = $Kernel::OM->Get('Kernel::System::User');
$ActionParams{GroupObject} = $Kernel::OM->Get('Kernel::System::Group');

#my $Action = "AgentTicketClose";
#my $Action = "AgentTicketFreeText";
#my $Action = "AgentTicketNote";
#my $Action = "AgentTicketOwner";
#my $Action = "AgentTicketPending";
#my $Action = "AgentTicketPriority";
#my $Action = "AgentTicketReponsible";

$ActionParams{LayoutObject}->mock('PopupClose' => sub {
	my ($Self, %Param) = @_;
	print STDERR "Layout PopupClose()\n";
	return values %Param;
});

$ActionParams{LayoutObject}->mock('Header' => sub {
    return "";
});
$ActionParams{LayoutObject}->mock('NavigationBar' => sub {
    return "";
});
$ActionParams{LayoutObject}->mock('Footer' => sub {
    return "";
});
$ActionParams{LayoutObject}->mock('Output' => sub {
	return "";
});

$ActionParams{ParamObject}->{Query}->mock('param' => sub {
    my ($Self, $ParamName) = @_;
    
    if (defined($ParamName)) {
        if (!exists($$ParamsRef{$ParamName})) {
            if (($ParamName eq "InformUserID") or ($ParamName eq "InvolvedUserID")) {
                print STDERR "GetArray() $ParamName not exists\n";
                return ();
            } else {
                print STDERR "GetParam() $ParamName not exists\n";
                return undef;
            }
        }
        return $$ParamsRef{$ParamName};
    } else {
        print STDERR "GetParamNames()\n";
        return keys %$ParamsRef;
    }
});

local $Kernel::AC = Kernel::Modules::AgentTicketActionCommon->new(%ActionParams);
$Kernel::AC = Test::MockObject::Extends->new($Kernel::AC);
$Kernel::AC->mock('_Mask' => sub {
	my ($Self, %Param) = @_;
	print STDERR "_Mask()\n";
	while (my ($key, $value) = each(%Param)) {
		if (defined $value && index($value, "Error") != -1) {
	        	print STDERR " Message: $key: $value\n";
		}
    }

	return "";
});
#$Kernel::AC->{TicketID} = $AcionParams{TicketID};
#$Kernel::AC->{UserID} = $AcionParams{UserID};
#$Kernel::AC->{Subaction} = "Store";
#$Kernel::AC->{UserFirstname} = $AcionParams{UserFirstname};
#$Kernel::AC->{UserLastname} = $AcionParams{UserLastname};
#$Kernel::AC->{UserEmail} = $AcionParams{UserEmail};

$Kernel::AC->{ConfigObject}->Set(
	Key => 'SessionCSRFProtection',
	Value => 0,
);

$Kernel::AC->Run();

my $lastFunctionCalled = $ActionParams{LayoutObject}->call_pos(-1);

if ($lastFunctionCalled eq "PopupClose") {
	print 1;
} else {
	print 0;
}
