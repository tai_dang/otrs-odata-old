#!/usr/bin/perl

use strict;
use warnings;
use JSON;

use lib '/opt/otrs';

use Kernel::System::ObjectManager;

my $TicketID = shift;

# create object manager
local $Kernel::OM = Kernel::System::ObjectManager->new(
    'Kernel::System::Log' => {
        LogPrefix => 'OTRS-otrs.get_article.pl',
    },
);

my $TicketObject = $Kernel::OM->Get('Kernel::System::Ticket');

my @articleResult = $TicketObject->ArticleGet(
	TicketID => $TicketID,
);

#my $json = encode_json \@articleResult;
my $json = JSON->new->utf8->encode(\@articleResult);

print $json;
