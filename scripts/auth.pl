#!/usr/bin/perl

use strict;
use warnings;
use diagnostics;

use lib '/opt/otrs';
use Kernel::System::ObjectManager;

my $Username = shift;
my $Password = shift;

# create object manager
local $Kernel::OM = Kernel::System::ObjectManager->new(
    'Kernel::System::Log' => {
        LogPrefix => 'OTRS-otrs.auth.pl',
    },
);

my $AuthObject = $Kernel::OM->Get('Kernel::System::Auth');

if ( $AuthObject->Auth( User => $Username, Pw => $Password ) ) {
    print 1;
}
else {
    print 0;
}
