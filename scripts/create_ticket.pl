#!/usr/bin/perl

use strict;
use warnings;
use diagnostics;
use Test::MockObject;
use Test::MockObject::Extends;
use JSON;

use lib '/opt/otrs';

use Kernel::System::ObjectManager;
use Kernel::Modules::AgentTicketPhone;

my @lines;
while (<>) {
    s/\s+\z//;
    push @lines, $_;
}

my $input = join " ", @lines;

my $ParamsRef = JSON->new->utf8->decode($input);

my %ActionParams = %{$$ParamsRef{ActionParams}};
delete $$ParamsRef{ActionParams};

# create object manager
local $Kernel::OM = Kernel::System::ObjectManager->new(
    'Kernel::System::Log' => {
        LogPrefix => 'OTRS-otrs.create_note.pl',
    },
);

$ActionParams{ParamObject} = $Kernel::OM->Get('Kernel::System::Web::Request');
$ActionParams{ParamObject}->{Query} = Test::MockObject::Extends->new($ActionParams{ParamObject}->{Query});
$ActionParams{DBObject} = $Kernel::OM->Get('Kernel::System::DB');
$ActionParams{TicketObject} = $Kernel::OM->Get('Kernel::System::Ticket');
my $LayoutObject = $Kernel::OM->Get('Kernel::Output::HTML::Layout');
$ActionParams{LayoutObject} = Test::MockObject::Extends->new($LayoutObject);
$ActionParams{LogObject} = $Kernel::OM->Get('Kernel::System::Log');
$ActionParams{QueueObject} = $Kernel::OM->Get('Kernel::System::Queue');
$ActionParams{MainObject} = $Kernel::OM->Get('Kernel::System::Main');
$ActionParams{ConfigObject} = $Kernel::OM->Get('Kernel::Config');
$ActionParams{UserObject} = $Kernel::OM->Get('Kernel::System::User');
$ActionParams{GroupObject} = $Kernel::OM->Get('Kernel::System::Group');

$ActionParams{LayoutObject}->mock('Redirect' => sub {
	my ($Self, %Param) = @_;
	print STDERR "Layout Redirect()\n";
	my @urlParams = split "=", $Param{OP};
	return $urlParams[-1];
});

$ActionParams{LayoutObject}->mock('Header' => sub {
    return "";
});
$ActionParams{LayoutObject}->mock('NavigationBar' => sub {
    return "";
});
$ActionParams{LayoutObject}->mock('Footer' => sub {
    return "";
});
$ActionParams{LayoutObject}->mock('Output' => sub {
	return "";
});

$ActionParams{ParamObject}->{Query}->mock('param' => sub {
    my ($Self, $ParamName) = @_;
    
    if (defined($ParamName)) {
        if (!exists($$ParamsRef{$ParamName})) {
            print STDERR "GetParam() $ParamName not exists\n";
            return undef;
        }
        return $$ParamsRef{$ParamName};
    } else {
        print STDERR "GetParamNames()\n";
        return keys %$ParamsRef;
    }
});

local $Kernel::TicketPhone = Kernel::Modules::AgentTicketPhone->new(%ActionParams);
$Kernel::TicketPhone = Test::MockObject::Extends->new($Kernel::TicketPhone);
$Kernel::TicketPhone->mock('_MaskPhoneNew' => sub {
	my ($Self, %Param) = @_;
	print STDERR "_MaskPhoneNew()\n";
    if (defined $Param{Errors}) {
	my %errors = %{$Param{Errors}};
        while (my ($key, $value) = each(%errors)) {
            print STDERR " Message: $key: $value\n";
        }
    }
	return "";
});

$Kernel::TicketPhone->{ConfigObject}->Set(
	Key => 'SessionCSRFProtection',
	Value => 0,
);

my $ticketID = $Kernel::TicketPhone->Run();

my $lastFunctionCalled = $ActionParams{LayoutObject}->call_pos(-1);

if ($lastFunctionCalled eq "Redirect") {
	print STDERR "Ticket ID $ticketID created!";

	print $ticketID;
} else {
	print -1;
}
